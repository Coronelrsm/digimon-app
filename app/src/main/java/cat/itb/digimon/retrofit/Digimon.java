package cat.itb.digimon.retrofit;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.google.gson.annotations.SerializedName;

public class Digimon {


    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("img")
    private String img;

    @SerializedName("level")
    private String level;

    public Digimon(int id, String name, String img, String level) {
        this.id = id;
        this.name = name;
        this.img = img;
        this.level = level;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImg() {
        return img;
    }

    public String getLevel() {
        return level;
    }
}
