package cat.itb.digimon.retrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface DigimonService {

    String ALLDIGIMON = "api/digimon";

    @GET(ALLDIGIMON)
    Call<List<Digimon>> getAllDigimons();


    @GET("api/digimon/id/{idDigimon}")
    Call<List<Digimon>> getCertainDigimon(@Path("idDigimon") String digimonId);

}
