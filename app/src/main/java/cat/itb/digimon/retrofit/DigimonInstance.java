package cat.itb.digimon.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DigimonInstance {


    private static Retrofit retrofit;
    private static final String BASE_URL = "https://digimon-api.herokuapp.com/"; //les comandes es fan amb estrings que s'afegeixen al final del BASE_URL

    public static Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
