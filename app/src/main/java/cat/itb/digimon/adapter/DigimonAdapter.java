package cat.itb.digimon.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.digimon.R;
import cat.itb.digimon.retrofit.Digimon;

public class DigimonAdapter extends RecyclerView.Adapter<DigimonAdapter.DigimonViewHolder> {

    onDigimonListener onDigimonListener;
    List<Digimon> digimonList;
    Context context;

    public void setOnDigimonListener(DigimonAdapter.onDigimonListener onDigimonListener) {
        this.onDigimonListener = onDigimonListener;
    }

    public DigimonAdapter(List<Digimon> digimonList, Context context) {
        this.digimonList = digimonList;
        this.context = context;
    }

    public void setDigimonList(List<Digimon> digimonList) {
        this.digimonList = digimonList;
        notifyDataSetChanged();

    }


    public class DigimonViewHolder extends RecyclerView.ViewHolder {


    private TextView digimonName;
    //private ImageView digimonPicture;

    public DigimonViewHolder(@NonNull View itemView) {
        super(itemView);
        itemView.setOnClickListener(this::digimonClicked);   //FALTAVA AIXÒ, cada viewholder o fila té una implementació de la interfície
        digimonName = itemView.findViewById(R.id.digimonName);
        //digimonPicture = itemView.findViewById(R.id.digimonPicture);

    }

        private void digimonClicked(View view) {
            Digimon digimon = digimonList.get(getAdapterPosition());
            onDigimonListener.onDigimonClicked(digimon);
        }


    }

    @NonNull
    @Override
    public DigimonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.digimon_row, parent, false);
        return new DigimonViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull DigimonViewHolder holder, int position) {

        Digimon digimon = digimonList.get(position);
        holder.digimonName.setText(digimon.getName());
        //holder.digimonPicture.setImageDrawable(digimon.getImg());

    }

    @Override
    public int getItemCount() {
        return digimonList.size();
    }

    public interface onDigimonListener{
        void onDigimonClicked(Digimon digimon);
    }


}
