package cat.itb.digimon.screens.mainScreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.digimon.R;

public class MainScreenFragment extends Fragment {

    @BindView(R.id.btnShowAllDigimon)
    Button btnShowAllDigimon;
    @BindView(R.id.textChoose)
    TextView textChoose;
    @BindView(R.id.editText)
    EditText editText;
    @BindView(R.id.btnSearch)
    Button btnSearch;
    private MainScreenViewModel mViewModel;
    View globalView;

    public static MainScreenFragment newInstance() {
        return new MainScreenFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_screen_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        globalView = view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MainScreenViewModel.class);



    }


    private void navigateToDigimonList() {
        NavDirections action = MainScreenFragmentDirections.navigateToDigimonList();
        Navigation.findNavController(globalView).navigate(action);
    }

    @OnClick({R.id.btnShowAllDigimon, R.id.btnSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnShowAllDigimon:
                navigateToDigimonList();
                break;
            case R.id.btnSearch:
                navigateToDetails();
                break;
        }
    }

    private void navigateToDetails() {
        NavDirections action = MainScreenFragmentDirections.navigateToDetails(String.valueOf(editText));
        Navigation.findNavController(globalView).navigate(action);
    }
}
