package cat.itb.digimon.screens.digimonList;

import androidx.lifecycle.ViewModel;

import cat.itb.digimon.retrofit.DigimonInstance;
import cat.itb.digimon.retrofit.DigimonService;

public class DigimonListViewModel extends ViewModel {

    DigimonService digimonService = DigimonInstance.getRetrofit().create(DigimonService.class);

    public DigimonService getDigimonInstance() {
        return digimonService;
    }
    // TODO: Implement the ViewModel
}
