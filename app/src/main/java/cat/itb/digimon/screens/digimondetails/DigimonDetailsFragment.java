package cat.itb.digimon.screens.digimondetails;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cat.itb.digimon.R;
import cat.itb.digimon.retrofit.Digimon;
import cat.itb.digimon.retrofit.DigimonInstance;
import cat.itb.digimon.retrofit.DigimonService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DigimonDetailsFragment extends Fragment {

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.picture)
    ImageView picture;
    @BindView(R.id.level)
    TextView level;
    private DigimonDetailsViewModel mViewModel;

    private String _id;

    private String idViaDirectSearch;



    private DigimonService digimonService;
    Call<List<Digimon>> digimonCall;
    Digimon digimon;

    public static DigimonDetailsFragment newInstance() {
        return new DigimonDetailsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.digimon_details_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        _id = DigimonDetailsFragmentArgs.fromBundle(getArguments()).getIdDigimon();
       // idViaDirectSearch = DigimonDetailsFragmentArgs.fromBundle(getArguments()).getIdDigimon();



    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(DigimonDetailsViewModel.class);





        digimonService = DigimonInstance.getRetrofit().create(DigimonService.class);
        digimonCall = digimonService.getCertainDigimon(_id);
        digimonCall.enqueue(new Callback<List<Digimon>>() {
            @Override
            public void onResponse(Call<List<Digimon>> call, Response<List<Digimon>> response) {
                digimon = response.body().get(0);

                name.setText(digimon.getName());

                loadImageFromUrl(digimon.getImg());
                
                level.setText(digimon.getLevel());

            }

            private void loadImageFromUrl(String img) {
                Picasso.get().load(img).into(picture);
            }

            @Override
            public void onFailure(Call<List<Digimon>> call, Throwable t) {

            }
        });

    }

}
