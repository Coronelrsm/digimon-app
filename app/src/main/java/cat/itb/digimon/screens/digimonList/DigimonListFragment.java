package cat.itb.digimon.screens.digimonList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cat.itb.digimon.R;
import cat.itb.digimon.adapter.DigimonAdapter;
import cat.itb.digimon.retrofit.Digimon;
import cat.itb.digimon.retrofit.DigimonService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DigimonListFragment extends Fragment {

    @BindView(R.id.digimon_recycler_view)
    RecyclerView digimonRecyclerView;
    private DigimonListViewModel mViewModel;

    private DigimonAdapter digimonAdapter;
    private LinearLayoutManager layoutManager;
    DigimonService digimonService;
    private View globalView;

    Call<List<Digimon>> digimonCallList;
    private List<Digimon> digimonList;

    public static DigimonListFragment newInstance() {
        return new DigimonListFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.digimon_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);


        digimonList = new ArrayList<>();
        digimonAdapter = new DigimonAdapter(digimonList, getContext());

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(DigimonListViewModel.class);

        digimonService = mViewModel.getDigimonInstance();
        digimonRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this.getActivity());
        digimonRecyclerView.setLayoutManager(layoutManager);
        digimonRecyclerView.setAdapter(digimonAdapter);

        digimonCallList = digimonService.getAllDigimons();
        digimonCallList.enqueue(new Callback<List<Digimon>>() {
            @Override
            public void onResponse(Call<List<Digimon>> call, Response<List<Digimon>> response) {
            digimonList = response.body();
            digimonAdapter.setDigimonList(digimonList);

            }

            @Override
            public void onFailure(Call<List<Digimon>> call, Throwable t) {
                throw new RuntimeException(t);
            }
        });

        digimonAdapter.setOnDigimonListener(this::checkDetails);

    }

    private void checkDetails(Digimon digimon) {
        NavDirections action = DigimonListFragmentDirections.navigateToDetails(String.valueOf(digimon.getId()));
        Navigation.findNavController(digimonRecyclerView).navigate(action);
    }

}
